var riot = require('riot')
var appMain = require('./app-main.tag')

// globally-available utils
window.C = require("./config.coffee")
window.U = require("./utils.coffee")

// site sections
require("./accounts.tag")

// services
require("./session.tag")

riot.mount('app-main', appMain)
