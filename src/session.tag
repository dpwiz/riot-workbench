session-store
  script.
    console.debug 'session-store', window.localStorage.sid

    ws = new ReconnectingWebSocket C.ws.url, null,
      automaticOpen: false
      debug: C.ws.debug || false

    ws_send = (msg) ->
      ws.send JSON.stringify msg

    ws.onopen = () ->
      ws_send
        sid: window.localStorage.sid || ''
      # wait for 'session:ready' next

    ws.onmessage = (e) ->
      try
        msg = JSON.parse e.data
      catch e
        console.warn 'ws', 'bad data:', e
        return

      if msg.sid?
        U.bus.trigger 'session:ready', msg
        return

      if msg.jsonrpc == '2.0'
        jsonrpc2 msg
        return

      console.warn 'ws', 'unexpected ws message', msg

    jsonrpc2 = (msg) ->
      if msg.method?
        U.bus.trigger "rpc:\#{msg.method}", msg.params, msg.id
        return
      if msg.id? and msg.result?
        U.bus.trigger "rpc:\#{msg.id}:result", msg.result
        return
      if msg.id? and msg.error?
        U.bus.trigger "rpc:\#{msg.id}:error", msg.error
        return
      if msg?.length?
        for item in msg
          jsonrpc2 item
        return
      console.warn 'ws', 'bad jsonrpc message', msg

    U.bus.on 'rpc:call', (method, params, on_result, on_error) ->
      id = 'call-' + Math.random().toString().slice(2)

      U.bus.one "rpc:\#{id}:result", (res) ->
        U.bus.off "rpc:\#{id}:error"
        on_result res

      U.bus.one "rpc:\#{id}:error", (err) ->
        U.bus.off "rpc:\#{id}:result"
        console.warn 'rpc', 'error', id, method, params, err
        on_error err

      ws_send
        jsonrpc: '2.0'
        id: id
        method: method
        params: params || {}

    U.bus.on 'rpc:notice', (method, params) ->
      ws_send
        jsonrpc: '2.0'
        method: method
        params: params || {}

    U.bus.on 'rpc:reply:result', (id, result) ->
      ws_send
        jsonrpc: '2.0'
        result: result || {}

    U.bus.on 'rpc:reply:error', (id, code, message, data) ->
      ws_send
        jsonrpc: '2.0'
        error:
          code: parseInt code, 10
          message: message || ''
          data: data || null

    U.bus.on 'rpc:batch', (messages) ->
      console.error 'ws', 'rpc', 'I do not trust you to send batches yet. Sorry.'

    # This is so complicated only because I do not want
    # to open socket before any auth action is requested.
    # Normal actions should rpc:call for themselves.
    U.bus.on 'auth:login', (form, callback) ->
      console.debug 'session', 'login:form', form

      U.bus.one 'session:ready', (msg) ->
        # Ignore msg.auth for now...
        U.bus.trigger 'rpc:call', 'login', form,
          (res) ->
            console.debug 'session', 'login:result', res
            if res
                window.localStorage.sid = msg.sid
                U.bus.trigger 'auth:success'
            else
                ws.close 1000, 'sorry'
            callback res

          (err) ->
            console.debug 'session', 'login:error', err
            ws.close 1000, 'oops'
            callback null

      ws.open()

    if !window.localStorage.sid
      U.bus.trigger 'auth:fail'
    else
      console.debug 'session:verify'
      U.bus.on 'session:ready', (msg) ->
        console.debug 'session:verify', msg
        if msg.auth
          window.localStorage.sid = msg.sid
          U.bus.trigger 'auth:success'
        else
          delete window.localStorage.sid
          U.bus.trigger 'auth:fail'
          ws.close 1000, 'too bad'
      ws.open()
