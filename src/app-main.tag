app-main
  layout-loading(each="{ (layout == 'loading')?[1]:[] }")
  layout-anonymous(each="{ (layout == 'anonymous')?[1]:[] }")
  layout-authenticated(each="{ (layout == 'authenticated')?[1]:[] }")
  session-store
  script.
    riot.route '/', () =>
      console.debug 'route', 'root'
    riot.route '/accounts', () =>
      console.debug 'route', 'accounts'
    riot.route.start true

    @layout = 'loading'

    U.bus.on 'auth:success', () =>
      @layout = 'authenticated'
      @update()
      riot.route '/app'

    U.bus.on 'auth:fail', () =>
      @layout = 'anonymous'
      @update()
      riot.route '/accounts/login'

  style.
    body {
      background: white;
        font-face: 'Open Sans';
        font-weight: 300;

        h1, h2, h3, h4, h5, h6 {
          font-weight: 700;
        }
    }

layout-loading
  .container-fluid
    p(align="center") loading...

layout-anonymous
  .container
    accounts-login(show="{ page == 'login' }")
    accounts-register(show="{ page == 'register' }")
    accounts-reset(show="{ page == 'reset' }")
  script.
    @page = 'login'

    route = riot.route.create()
    route '/accounts/*', (page) =>
      @page = page
      @update()

layout-authenticated
  .container
    app-header
    p welcome, { parent.user.sid }

app-header
  h1 My great app
  h2 It's great.

  style(scoped).
    :scope {
      border-bottom: 1px solid black;
    }
