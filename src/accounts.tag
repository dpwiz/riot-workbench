accounts-login
  .row
    .col-sm-6.col-sm-offset-3
      .card(style="margin-top: 2em;")
        img.card-img-top(src="https://placehold.it/538x300" style="width: 100%; height: auto;")
        .card-block
          h4.card-title Log in
          .card-subtitle.text-muted Thats the easiest part.
          br
          .form-group
            input.form-control(
              type="text"
              onchange="{ set('login') }"
              placeholder="E-mail"
              __disabled="{ loading }"
            )
          .form-group
            input.form-control(
              type="password"
              onchange="{ set('password') }"
              placeholder="Password"
              __disabled="{ loading }"
            )
          .form-group
            button.btn.btn-primary(
              onclick="{ login }"
              __disabled="{ loading }"
            ) Log in
            button.btn.btn-link(disabled show="{ loading }") Loading...
          hr
          a.card-link(href="#accounts/register") Register
          a.card-link(href="#accounts/reset") Reset password
  script.
    @loading = false
    @form = {}

    @set = (name) =>
      (e) =>
        @form[name] = e.target.value
        return true

    @login = () =>
      console.debug 'accounts', 'login form', @form
      return unless @form.login and @form.password

      @loading = true
      @update()

      U.bus.trigger 'auth:login', @form, (res) =>
        @loading = false
        @update()

accounts-register
  .row
    .col-sm-6.col-sm-offset-3
      .card(style="margin-top: 2em;")
        img.card-img-top(src="https://placehold.it/538x300" style="width: 100%; height: auto;")
        .card-block
          h4.card-title Register
          .card-subtitle.text-muted Give us your personals. please?..
          hr
          a.card-link(href="#accounts/login") Login
          a.card-link(href="#accounts/reset") Reset password

accounts-reset
  .row
    .col-sm-6.col-sm-offset-3
      .card(style="margin-top: 2em;")
        img.card-img-top(src="https://placehold.it/538x300" style="width: 100%; height: auto;")
        .card-block
          h4.card-title Password reset
          .card-subtitle.text-muted Ugh... have you tried clearing cache instead?
          hr
          a.card-link(href="#accounts/login") Login
          a.card-link(href="#accounts/register") Register
