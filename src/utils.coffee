do (require, module) ->
  riot = require('riot')

  module.exports =
    bus: riot.observable()
