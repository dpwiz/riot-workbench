### Setting up

``` bash
git clone git@bitbucket.org:dpwiz/riot-workbench.git
cd riot-workbench
npm install
```

### Working

``` bash
npm run build # one-off build. add extra params after --
npm run dist  # with minifaction adnd sourcemaps
npm run watch # watch me rebuild this!
```

Serve ~~chilled~~ `dist/` from any http server.
